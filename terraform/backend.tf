terraform {
  backend "s3" {
    bucket = "cns-cicd-bucket"
    key    = "cns-cicd-case1.terraform.tfstate"
    region = "ap-northeast-2"
  }
}

